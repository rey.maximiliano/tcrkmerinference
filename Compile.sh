mkdir -p bin
rm -f bin/*.class
cd src

javac ConvertInProbabilities.java Counter.java CountersGroup.java PairsCounter.java Predictor.java PredictorPerPosition.java Test2Secuences.java Test.java Trainer.java Tuple.java Utils.java

mv *.class ../bin/

cd ..
