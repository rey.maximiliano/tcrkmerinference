import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Counter<T> {
	private Map<T,Integer> map;
	private int total;
	public Counter() {
		this.map = new HashMap<T,Integer>();
	}
	public void addOne(T elem) {
		Integer q = map.get(elem);
		total = total + 1;
		if(q==null) {
			map.put(elem, 1);
		}else {
			map.put(elem, q+1);
		}
	}
	public int quantity(T elem) {
		Integer res = map.get(elem);
		if(res==null) {
			return 0;
		}else {
			return res;
		}
	}

	Iterator<Entry<T,Integer> > iterator(){
		return this.map.entrySet().iterator();
	}
	public int getTotal(){
		return total;
	}
}
