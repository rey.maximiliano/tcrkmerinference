import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Trainer {
	public static CountersGroup sliceInKmeros(Map<String,Set<String>> entry,int k) {
		CountersGroup res = new CountersGroup();
		for(Iterator<Entry<String,Set<String>>> it = entry.entrySet().iterator();it.hasNext() ;) {
			Entry<String,Set<String>> elem = it.next();
			for(Iterator<String> it2 = elem.getValue().iterator(); it2.hasNext();) {
				String tcr = it2.next();
				for(int i = 0;i<=tcr.length()-k;i+=1) {
					String kmero = tcr.substring(i, i+k);
					res.antigenKmeroCounter.addOne(new Tuple<String,String>(elem.getKey(), kmero));
					res.kmeroCounter.addOne(kmero);
				}
			}
		}
		return res;
	}
	public static Map<Tuple<String,String>,Double> calculateProbabilities(CountersGroup entry) {
		Map<Tuple<String,String>,Double> res = new LinkedHashMap<Tuple<String, String>, Double>();
		for(Iterator<Entry<Tuple<String,String>,Integer>> it = entry.antigenKmeroCounter.iterator();it.hasNext();) {
			Entry<Tuple<String,String>,Integer> actual = it.next();
			int kmersProb = entry.kmeroCounter.quantity(actual.getKey().b);
			res.put(actual.getKey(), Double.valueOf(actual.getValue())/Double.valueOf(kmersProb));
		}
		return res;
	}
	
	public static void trainAndSave(Map<String,Set<String>> train, String outputPath, int k) throws IOException {
		CountersGroup counterGroup = sliceInKmeros(train,k);
		Map<Tuple<String,String>,Double> res = calculateProbabilities(counterGroup);
		BufferedWriter output = new BufferedWriter(new FileWriter(new File(outputPath)));
		output.write(Integer.toString(k));
		output.write("\n");
		for(Iterator<Map.Entry<Tuple<String,String>,Double>> it = res.entrySet().iterator(); it.hasNext();) {
			Map.Entry<Tuple<String,String>,Double> actual = it.next();
			output.write(actual.getKey().a + " " + actual.getKey().b + " " + actual.getValue() + "\n");
		}
		output.close();
	}
	
	public static void main(String[] argv) throws IOException {
		if(argv.length == 0) {
			System.err.print("<number of chains>");
			return;
		}
		if( Integer.parseInt(argv[0]) == 1) {
			if(argv.length < 4) {
				System.err.print("1 <path of train set> <path to output> <kmers length>");
				return;
			}
			String trainPath = argv[1];
			String outputPath = argv[2];
			int k = Integer.parseInt(argv[3]);
			Tuple<Map<String,Set<String>> ,Set<String> > train = Utils.loadDatasetOneChain(trainPath);
			trainAndSave(train.a,outputPath,k);
		}else if(Integer.parseInt(argv[0]) == 2) {
			if(argv.length < 6) {
				System.err.print("2 <path of train set> <path to output alphas> <path to output betas> <kmers alphas length> <kmers beta length>");
				return;
			}
			String trainPath = argv[1];
			String outputPathAlpha = argv[2];
			String outputPathBeta = argv[3];
			int kAlpha = Integer.parseInt(argv[4]);
			int kBeta = Integer.parseInt(argv[5]);
			
			Tuple<Map<String,Set<String>>, Tuple<Map<String,Set<String>>,Set<String>>> train = Utils.loadDatabaseTwoChains(trainPath);
			
			trainAndSave(train.a,outputPathAlpha,kAlpha);
			trainAndSave(train.b.a,outputPathBeta,kBeta);
		}else {
			System.err.print("Only support one or two chains");
		}
		
	}
}