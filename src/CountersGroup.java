
public class CountersGroup {
	public Counter<Tuple<String,String> > antigenKmeroCounter;
	public Counter<String > kmeroCounter;
	public CountersGroup() {
		this.antigenKmeroCounter = new Counter<Tuple<String, String>>();
		this.kmeroCounter = new Counter<String>();
	}
}
