import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

public class Test2Secuences {
	public static void main(String[] argv) throws IOException {
		if(argv.length < 5) {
			System.err.println("<Train set file> <Test set file> <kmer alpha length> <kmer beta length> <predictionOutput> <timeRegister>");
			return;
		}
		String train = argv[0];
		String test = argv[1];
		int kAlphas = Integer.parseInt(argv[2]);
		int kBetas = Integer.parseInt(argv[3]);
		BufferedWriter predictionOutput = new BufferedWriter(new FileWriter(argv[4],true));
		
		Tuple<Map<String,Set<String>>, Tuple<Map<String,Set<String>>,Set<String>>> loadedTrain = Utils.loadDatabaseTwoChains(train);
		long antigenDatabaseLength = Utils.antigenDatabaseLength;
		Map<String,Set<String>> trainSetAlphas = loadedTrain.a; 
		Map<String,Set<String>> trainSetBetas = loadedTrain.b.a; 
		Set<String> antigens = loadedTrain.b.b;
		
		long timePreTraining = System.currentTimeMillis();
		Map<Tuple<String,String>,Double> trainedAlhas = Trainer.calculateProbabilities(Trainer.sliceInKmeros(trainSetAlphas, kAlphas));
		Map<Tuple<String,String>,Double> trainedBetas = Trainer.calculateProbabilities(Trainer.sliceInKmeros(trainSetBetas, kBetas));
		long timePostTraining = System.currentTimeMillis();
		
		BufferedReader testFile = new BufferedReader(new FileReader(new File(test)));
		String line = testFile.readLine();
		int goodCounter = 0;
		int total = 0;
		long timePrediction = 0;
		while(line!=null) {
			String[] lineS = line.split(" ");
			long timePrePrediction = System.currentTimeMillis();
			SortedSet<Map.Entry<String,Double>> resultPrediction = Predictor.combinatePredictions(Predictor.getProbabilitiesOfTCR(lineS[1], kAlphas, trainedAlhas, antigens),Predictor.getProbabilitiesOfTCR(lineS[2], kBetas, trainedBetas, antigens));
			long timePostPrediction = System.currentTimeMillis();
			Iterator<Map.Entry<String,Double> > it = resultPrediction.iterator();
			Map.Entry<String,Double> firstPrediction = it.next();
			Map.Entry<String,Double> secondPrediction = it.next();
			timePrediction += timePostPrediction - timePrePrediction;
			if(lineS[0].equals(firstPrediction.getKey())) {
				goodCounter++;
			}
			total++;
			predictionOutput.write(Integer.toString(kAlphas));
			predictionOutput.write(" ");
			predictionOutput.write(Integer.toString(kBetas));
			predictionOutput.write(" ");
			predictionOutput.write(firstPrediction.getKey());
			predictionOutput.write(" ");
			predictionOutput.write(lineS[0]);
			predictionOutput.write(" ");
			predictionOutput.write(Double.toString(firstPrediction.getValue()/secondPrediction.getValue()));
			predictionOutput.write('\n');
			line = testFile.readLine();
		}
		predictionOutput.close();
		System.out.print(kAlphas);
		System.out.print(" ");
		System.out.print(kBetas);
		System.out.print(" ");
		System.out.println(Double.valueOf(goodCounter) * 100.0 / Double.valueOf(total));
		
		if(argv.length >= 6) {
			BufferedWriter timeRegister = new BufferedWriter(new FileWriter(argv[5],true));
			timeRegister.write("KMER_ALPHA_LENGTH: ");
			timeRegister.write(Integer.toString(kAlphas));
			timeRegister.write(" KMER_BETA_LENGTH: ");
			timeRegister.write(Integer.toString(kBetas));
			timeRegister.write(" TIME_TRAINING: ");
			timeRegister.write(Long.toString(timePostTraining - timePreTraining));
			timeRegister.write(" LENGTH_TRAIN_DATABASE: ");
			timeRegister.write(Long.toString(antigenDatabaseLength));
			timeRegister.write(" TIME_PREDICTION: ");
			timeRegister.write(Long.toString(timePrediction));
			timeRegister.write(" LENGTH_TEST_DATABASE: ");
			timeRegister.write(Integer.toString(total));
			timeRegister.write("\n");
			timeRegister.close();
		}
	}
}
