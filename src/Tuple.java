import java.util.Map.Entry;

public class Tuple<T,V> implements Entry<T,V>{
	public T a;
	public V b;
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		if(!this.getClass().equals(o.getClass())) {
			return false;
		}
		Tuple<Object,Object> anotherTuple = (Tuple<Object,Object>)o;
		return this.a.equals(anotherTuple.a) && this.b.equals(anotherTuple.b);
	}
	@Override
	public int hashCode() {
		return this.a.hashCode() + this.b.hashCode();
	}
	public Tuple(T elem1, V elem2) {
		this.a = elem1;
		this.b = elem2;
	}
	@Override
	public T getKey() {
		return this.a;
	}
	@Override
	public V getValue() {
		return this.b;
	}
	@Override
	public V setValue(V value) {
		V res = this.b;
		this.b = value;
		return res;
	}
}
