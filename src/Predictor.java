import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Predictor {
	public static class MaxScoreCompare implements Comparator<Map.Entry<String,Double>>{
		@Override
		public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
			if(o1.getValue().equals(o2.getValue())) {
				return o1.getKey().compareTo(o2.getKey());
			}else if(o1.getValue() < o2.getValue()) {
				return 1;
			}else {
				return -1;
			}
		}
		public static MaxScoreCompare comparator = new MaxScoreCompare();
	}
	
	public static Map<String,Double> getProbabilitiesOfTCR(String tcr, int k, Map<Tuple<String,String>,Double> prob, Set<String> antigens){
		Map<String,Double> res = new LinkedHashMap<String, Double>();
		for(Iterator<String> it = antigens.iterator(); it.hasNext();) {
			String antigen = it.next();
			res.put(antigen, 0.0);
		}
		for(int i = 0;i<=tcr.length()-k;i+=1) {
			String kmero = tcr.substring(i, i+k);
			for(Iterator<String> it = antigens.iterator(); it.hasNext();) {
				String antigen = it.next();
				res.put(antigen, res.get(antigen) + prob.getOrDefault(new Tuple<String,String>(antigen,kmero),0.0)/(tcr.length()-k + 1) );
			}
		}
		return res;
	}
	public static SortedSet<Map.Entry<String,Double>> findMax(Map<String,Double> probOfTCR) {
		SortedSet<Map.Entry<String,Double>> res = new TreeSet<Map.Entry<String,Double>>(MaxScoreCompare.comparator);
		for(Iterator<Map.Entry<String, Double>> it = probOfTCR.entrySet().iterator();it.hasNext();) {
			Map.Entry<String, Double> entry = it.next();
			res.add(entry);
		}
		return res;
	}
	public static SortedSet<Map.Entry<String,Double>> combinatePredictions(Map<String,Double> alphas, Map<String,Double> betas){
		SortedSet<Map.Entry<String,Double>> res = new TreeSet<Map.Entry<String,Double>>(MaxScoreCompare.comparator);
		for(Iterator<Map.Entry<String, Double>> it = alphas.entrySet().iterator();it.hasNext();) {
			Map.Entry<String, Double> entry = it.next();
			double p = entry.getValue() + betas.get(entry.getKey());
			res.add(new Tuple<String,Double>(entry.getKey(),p/2.0));
		}
		return res;
	}
	
	public static Tuple<Map<Tuple<String,String>,Double>,Tuple<Integer,Set<String>> > loadTrainOutput(String path) throws IOException{
		BufferedReader trainOutput = new BufferedReader(new FileReader(new File(path)));
		String line = trainOutput.readLine();
		int k = Integer.parseInt(line);
		line = trainOutput.readLine();
		Set<String> antigens = new LinkedHashSet<String>();
		Map<Tuple<String,String>,Double> res = new LinkedHashMap<Tuple<String,String>,Double>(); 
		while(line!=null) {
			String[] lineS = line.split(" ");
			res.put(new Tuple<String,String>(lineS[0],lineS[1]),Double.parseDouble(lineS[2]));
			antigens.add(lineS[0]);
			line = trainOutput.readLine();
		}
		trainOutput.close();
		return new Tuple<Map<Tuple<String,String>,Double>,Tuple<Integer,Set<String>> >(res,new Tuple<Integer,Set<String>>(k,antigens));
	}
	
	public static void main(String[] argv) throws IOException {
		if(argv.length <3 || (!argv[0].equals("tcrs") && !argv[0].equals("file")) ) {
			System.err.println("tcrs 1 <train output> <tcr1> ... <tcrn>");
			System.err.println("file 1 <train output> <test file>");
			System.err.println("tcrs 2 <train output alpha> <train output beta> <tcr1-alpha> <tcr1-beta> ... <tcrn-alpha> <tcrn-beta>");
			System.err.println("file 2 <train output alpha> <train output beta> <test file>");
			return;
		}
		
		if(Integer.parseInt(argv[1]) == 1) {
			Tuple<Map<Tuple<String,String>,Double>,Tuple<Integer,Set<String>> > loaded = loadTrainOutput(argv[2]);
			Map<Tuple<String,String>,Double> prob = loaded.a;
			int k = loaded.b.a;
			Set<String> antigens = loaded.b.b;
			if(argv[0].equals("tcrs")) {
				for(int i = 3; i<argv.length;i+=1) {
					System.out.println(argv[i]);
					Map<String,Double> p = getProbabilitiesOfTCR(argv[i],k,prob,antigens);
					SortedSet<Map.Entry<String,Double>> res = findMax(p);
					for(Iterator<Map.Entry<String,Double>> it = res.iterator();it.hasNext();) {
						Map.Entry<String,Double> elem = it.next();
						System.out.println(elem.getKey() + " " + elem.getValue());
					}
					System.out.println();
				}
			}else {
				BufferedReader tcrs = new BufferedReader(new FileReader(new File(argv[3])));
				String line = tcrs.readLine();
				while(line!=null) {
					System.out.println(line);
					Map<String,Double> p = getProbabilitiesOfTCR(line,k,prob,antigens);
					SortedSet<Map.Entry<String,Double>> res = findMax(p);
					for(Iterator<Map.Entry<String,Double>> it = res.iterator();it.hasNext();) {
						Map.Entry<String,Double> elem = it.next();
						System.out.println(elem.getKey() + " " + elem.getValue());
					}
					System.out.println();
					line = tcrs.readLine();
				}
			}
		}else if(Integer.parseInt(argv[1]) == 2) {
			Tuple<Map<Tuple<String,String>,Double>,Tuple<Integer,Set<String>> > loadedAlpha = loadTrainOutput(argv[2]);
			Map<Tuple<String,String>,Double> probAlpha = loadedAlpha.a;
			int kAlpha = loadedAlpha.b.a;
			Set<String> antigens = loadedAlpha.b.b;
			
			Tuple<Map<Tuple<String,String>,Double>,Tuple<Integer,Set<String>> > loadedBeta = loadTrainOutput(argv[3]);
			Map<Tuple<String,String>,Double> probBeta = loadedBeta.a;
			int kBeta = loadedBeta.b.a;
			
			if(argv[0].equals("tcrs")) {
				for(int i = 4;i<argv.length;i+=2) {
					System.out.println(argv[i] + " " + argv[i+1]);
					Map<String,Double> pAlpha = getProbabilitiesOfTCR(argv[i],kAlpha,probAlpha,antigens);
					Map<String,Double> pBeta = getProbabilitiesOfTCR(argv[i+1],kBeta,probBeta,antigens);
					SortedSet<Map.Entry<String,Double>> res = combinatePredictions(pAlpha,pBeta);
					for(Iterator<Map.Entry<String,Double>> it = res.iterator();it.hasNext();) {
						Map.Entry<String,Double> elem = it.next();
						System.out.println(elem.getKey() + " " + elem.getValue());
					}
					System.out.println();
				}
			}else {
				BufferedReader tcrs = new BufferedReader(new FileReader(new File(argv[4])));
				String line = tcrs.readLine();
				while(line!=null) {
					System.out.println(line);
					String[] lineS = line.split(" ");
					Map<String,Double> pAlpha = getProbabilitiesOfTCR(lineS[0],kAlpha,probAlpha,antigens);
					Map<String,Double> pBeta = getProbabilitiesOfTCR(lineS[1],kBeta,probBeta,antigens);
					SortedSet<Map.Entry<String,Double>> res = combinatePredictions(pAlpha,pBeta);
					for(Iterator<Map.Entry<String,Double>> it = res.iterator();it.hasNext();) {
						Map.Entry<String,Double> elem = it.next();
						System.out.println(elem.getKey() + " " + elem.getValue());
					}
					System.out.println();
					line = tcrs.readLine();
				}
			}
		}else {
			System.err.println("System only supports one or two sequences");
		}
	}
}
