import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Utils {
	public static long antigenDatabaseLength = 0;
	public static Tuple<Map<String,Set<String>> ,Set<String> > loadDatasetOneChain(String path) throws IOException {
		BufferedReader trainFile = new BufferedReader(new FileReader(new File(path)));
		String line = trainFile.readLine();
		Map<String,Set<String>> trainSet = new LinkedHashMap<String,Set<String>>(); 
		Set<String> antigens = new LinkedHashSet<String> ();
		long _antigenDatabaseLength = 0;
		while(line!=null) {
			String[] lineS = line.split(" ");
			Set<String> v = trainSet.get(lineS[0]);
			antigens.add(lineS[0]);
			if(v==null) {
				v = new LinkedHashSet<String>();
				trainSet.put(lineS[0], v);
			}
			v.add(lineS[1]);
			line = trainFile.readLine();
			_antigenDatabaseLength += 1;
		}
		trainFile.close();
		antigenDatabaseLength = _antigenDatabaseLength;
		return new Tuple<Map<String,Set<String>> ,Set<String> > (trainSet,antigens);
	}
	
	public static Tuple<Map<String,Set<String>>, Tuple<Map<String,Set<String>>,Set<String>>> loadDatabaseTwoChains(String path) throws IOException {
		BufferedReader trainFile = new BufferedReader(new FileReader(new File(path)));
		String line = trainFile.readLine();
		Map<String,Set<String>> trainSetAlphas = new LinkedHashMap<String,Set<String>>(); 
		Map<String,Set<String>> trainSetBetas = new LinkedHashMap<String,Set<String>>(); 
		Set<String> antigens = new LinkedHashSet<String> ();
		long _antigenDatabaseLength = 0;
		while(line!=null) {
			String[] lineS = line.split(" ");
			Set<String> subsetAlphas = trainSetAlphas.get(lineS[0]);
			antigens.add(lineS[0]);
			if(subsetAlphas==null) {
				subsetAlphas = new LinkedHashSet<String>();
				trainSetAlphas.put(lineS[0], subsetAlphas);
			}
			subsetAlphas.add(lineS[1]);
			Set<String> subsetBetas = trainSetBetas.get(lineS[0]);
			if(subsetBetas==null) {
				subsetBetas = new LinkedHashSet<String>();
				trainSetBetas.put(lineS[0], subsetBetas);
			}
			subsetBetas.add(lineS[2]);
			line = trainFile.readLine();
			_antigenDatabaseLength += 1;
		}
		trainFile.close();
		antigenDatabaseLength = _antigenDatabaseLength;
		return new Tuple<Map<String,Set<String>>, Tuple<Map<String,Set<String>>,Set<String>>>(trainSetAlphas,new Tuple<Map<String,Set<String>>,Set<String>>(trainSetBetas,antigens)); 
	}
}
