import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

public class Test {
	public static void main(String[] argv) throws IOException {
		if(argv.length < 4) {
			System.err.println("<Train set file> <Test set file> <kmer length> <predictionOutput> <timeRegister>");
			return;
		}
		String train = argv[0];
		String test = argv[1];
		int k = Integer.parseInt(argv[2]);
		BufferedWriter predictionOutput = new BufferedWriter(new FileWriter(argv[3],true));
		Tuple<Map<String,Set<String>> ,Set<String> > trainLoad = Utils.loadDatasetOneChain(train);
		long antigenDatabaseLength = Utils.antigenDatabaseLength;
		Map<String,Set<String>> trainSet = trainLoad.a; 
		Set<String> antigens = trainLoad.b;
		long timePreTraining = System.currentTimeMillis();
		Map<Tuple<String,String>,Double> trained = Trainer.calculateProbabilities(Trainer.sliceInKmeros(trainSet, k));
		long timePostTraining = System.currentTimeMillis();
		BufferedReader testFile = new BufferedReader(new FileReader(new File(test)));
		String line = testFile.readLine();
		int goodCounter = 0;
		int total = 0;
		long timePrediction = 0;
		while(line!=null) {
			String[] lineS = line.split(" ");
			long timePrePrediction = System.currentTimeMillis();
			SortedSet<Map.Entry<String,Double>> resultPrediction = Predictor.findMax(Predictor.getProbabilitiesOfTCR(lineS[1], k, trained, antigens));
			long timePostPrediction = System.currentTimeMillis();
			timePrediction += timePostPrediction - timePrePrediction;
			Iterator<Map.Entry<String,Double> > it = resultPrediction.iterator();
			Map.Entry<String,Double> firstPrediction = it.next();
			Map.Entry<String,Double> secondPrediction = it.next();
			if(lineS[0].equals(firstPrediction.getKey())) {
				goodCounter++;
			}
			predictionOutput.write(Integer.toString(k));
			predictionOutput.write(" ");
			predictionOutput.write(firstPrediction.getKey());
			predictionOutput.write(" ");
			predictionOutput.write(lineS[0]);
			predictionOutput.write(" ");
			predictionOutput.write(Double.toString(firstPrediction.getValue()/secondPrediction.getValue()));
			predictionOutput.write("\n");
			total++;
			line = testFile.readLine();
		}
		predictionOutput.close();
		if(argv.length >= 5) {
			BufferedWriter timeRegister = new BufferedWriter(new FileWriter(argv[4],true));
			timeRegister.write("KMER_LENGTH: ");
			timeRegister.write(Integer.toString(k));
			timeRegister.write(" TIME_TRAINING: ");
			timeRegister.write(Long.toString(timePostTraining - timePreTraining));
			timeRegister.write(" LENGTH_TRAIN_DATABASE: ");
			timeRegister.write(Long.toString(antigenDatabaseLength));
			timeRegister.write(" TIME_PREDICTION: ");
			timeRegister.write(Long.toString(timePrediction));
			timeRegister.write(" LENGTH_TEST_DATABASE: ");
			timeRegister.write(Integer.toString(total));
			timeRegister.write("\n");
			timeRegister.close();
		}
		System.out.print(k);
		System.out.print(" ");
		System.out.println(Double.valueOf(goodCounter) * 100.0 / Double.valueOf(total));
	}
}
