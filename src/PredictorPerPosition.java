import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PredictorPerPosition {
	public static Map<String,Double> getProbabilitiesOfTCR(String tcr, int k, Map<Tuple<String,String>,Double> prob, Set<String> antigens){
		Map<String,Double[]> resPerPosition = new HashMap<String, Double[]>();
		for(Iterator<String> it = antigens.iterator(); it.hasNext();) {
			String antigen = it.next();
			Double[] arr = new Double[tcr.length()];
			for(int i = 0;i<tcr.length();i+=1) {
				arr[i] = 0.0;
			}
			resPerPosition.put(antigen, arr);
		}
		for(int i = 0;i<=tcr.length()-k;i+=1) {
			String kmero = tcr.substring(i, i+k);
			for(Iterator<String> it = antigens.iterator(); it.hasNext();) {
				String antigen = it.next();
				Double[] arr = resPerPosition.get(antigen);
				Double p = prob.getOrDefault(new Tuple<String,String>(antigen,kmero),0.0);
				for(int j = i; j<i+k;j+=1) {
					arr[j] = arr[j] + p;
				}
			}
		}
		Map<String,Double> res = new HashMap<String,Double> ();
		for(Iterator<Map.Entry<String,Double[]> > it = resPerPosition.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String,Double[]> entry = it.next();
			Double p = 1.0;
			for(int i = 0;i<tcr.length();i+=1) {
				p = p* entry.getValue()[i];
			}
			res.put(entry.getKey(), p);
		}
		return res;
	}
}
