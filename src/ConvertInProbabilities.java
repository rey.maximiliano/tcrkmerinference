import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class ConvertInProbabilities {
	public static void main(String[] argv) throws IOException {
		String train = argv[0];
		String antigenTarget = argv[1];
		int k = Integer.parseInt(argv[2]);
		BufferedReader trainFile = new BufferedReader(new FileReader(new File(train)));
		String line = trainFile.readLine();
		Map<String,Set<String>> trainSet = new LinkedHashMap<String,Set<String>>(); 
		Set<String> antigens = new LinkedHashSet<String> ();
		Set<String> kmeros = new LinkedHashSet<String> ();
		while(line!=null) {
			String[] lineS = line.split(" ");
			Set<String> v = trainSet.get(lineS[0]);
			antigens.add(lineS[0]);
			if(v==null) {
				v = new LinkedHashSet<String>();
				trainSet.put(lineS[0], v);
			}
			v.add(lineS[1]);
			kmeros.add(lineS[1]);
			line = trainFile.readLine();
		}
		trainFile.close();
		Map<Tuple<String,String>,Double> trained = Trainer.calculateProbabilities(Trainer.sliceInKmeros(trainSet, k));
		for(Iterator<Map.Entry<Tuple<String,String>,Double> > it = trained.entrySet().iterator(); it.hasNext();) {
			Map.Entry<Tuple<String,String>,Double>  entry = it.next();
			if(entry.getKey().a.equals(antigenTarget)) {
				System.out.print(entry.getKey().b);
				System.out.print(" ");
				System.out.println(entry.getValue());
			}
		}
	}
}
