import java.util.HashMap;
import java.util.Map;

public class PairsCounter<T,V> {
	private Map<T,Map<V,Integer>> map;
	PairsCounter(){
		this.map = new HashMap<T,Map<V,Integer>>();
	}
	public void addOne(T e1, V e2) {
		Map<V,Integer> internalMap = map.get(e1);
		if(internalMap==null) {
			internalMap = new HashMap<V, Integer>();
			map.put(e1, internalMap);
		}
		Integer number = internalMap.get(e2);
		if(number==null) {
			internalMap.put(e2, 1);
		}else {
			internalMap.put(e2, number + 1);
		}
	}
	public int quantity(T e1, V e2) {
		Map<V,Integer> internalMap = map.get(e1);
		if(internalMap==null) {
			return 0;
		}
		Integer number = internalMap.get(e2);
		if(number==null) {
			return 0;
		}else {
			return number;
		}
	}
}
